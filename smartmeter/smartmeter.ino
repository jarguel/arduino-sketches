#include <avr/sleep.h>
#include <avr/power.h>
#include <MsTimer2.h>
#include <QueueArray.h>
#include <XBee.h>
#include <SoftwareSerial.h>

#define P_DIODE            2
#define P_XBEE_CTS        11
#define P_XBEE_SLEEP_RQ   12
#define P_XBEE_RESET      14
#define P_LED             13

#define MESSAGE_SIZE      64
#define PAYLOAD_SIZE       8  //sizeof(payload_t)

#define SLOT_TIME_SEC           60  //Time resolution in seconds
#define NB_TX_TRY               1
#define NB_CONSECUTIVE_FAIL     5

#define SH_SMARTMETER_SAMPLE    0x10
#define SH_RECEIVER_ACK         0xAA

//---------------------
// Variables
//---------------------
// create the XBee object
XBee xbee = XBee();

//Serial for debug
SoftwareSerial debug(7, 8);

typedef struct {
  uint8_t   msgType;
  uint16_t  msgID;
  //TODO also need date
  uint8_t  nbSlot;   //TODO date, nbSlot and wattCnt must form a struct, this struct is going into the queue
  uint32_t  wattCnt;
} payload_t;

payload_t payload;
uint8_t buffer[PAYLOAD_SIZE];
uint8_t message[MESSAGE_SIZE];

XBeeAddress64       addr64     = XBeeAddress64(0x0013a200, 0x408695CD);
ZBTxRequest         dataTx     = ZBTxRequest(addr64, buffer, sizeof(buffer));
ZBTxRequest         msgTx      = ZBTxRequest(addr64, message, sizeof(message));
ZBTxStatusResponse  txStatus   = ZBTxStatusResponse();
ZBRxResponse        rx         = ZBRxResponse();
AtCommandRequest    atRequest  = AtCommandRequest();
AtCommandResponse   atResponse = AtCommandResponse();

uint32_t     wattCnt;
uint16_t     msgID;
bool         readyToSend;
bool         TxSuccess;
bool         TxPending;
uint8_t      TxTry;
uint8_t      nbFail;
uint8_t      check_ok;
QueueArray <uint32_t> queue;

//---------------------
// Functions
//---------------------
bool readPacket();

void flashLed(int pin, int times, int wait) {
  for (int i = 0; i < times; i++) {
    digitalWrite(pin, HIGH);
    delay(wait);
    digitalWrite(pin, LOW);
    if (i + 1 < times)
      delay(wait);
    }
}

inline void xbeeReset(void) {
  digitalWrite(P_XBEE_RESET, 0);
  delay(100);
  digitalWrite(P_XBEE_RESET, 1);
}

inline void xbeeWakeUp(void) {
  digitalWrite(P_XBEE_SLEEP_RQ, 0);
}

inline void xbeeWaitForReady(void) {
  while(digitalRead(P_XBEE_CTS));
  //TODO implement timeout
}

inline void xbeeSleep(void) {
  digitalWrite(P_XBEE_SLEEP_RQ, 1);
}

void isrFlash() {
  digitalWrite(P_LED, 1);
  wattCnt++;
  digitalWrite(P_LED, 0);
}

void isrTimer2() {
  //TODO for now queue size is increased when full, need to modify the lib to have fixed queue size
  //if(!queue.isFull())
  queue.push(wattCnt);
  readyToSend=true;
}


//---------------------
// Arduino setup
//---------------------
void setup() {
  
  //Init counters
  wattCnt=0;
  msgID=0;
  readyToSend=false;
  TxSuccess=false;
  TxPending=false;
  TxTry=0;
  
  memset(&payload, 0, sizeof(payload));
  
  debug.begin(9600);
  debug.println("\n\n**SmartMeter start**");

  //Configure pins
  pinMode(P_LED           , OUTPUT);
  pinMode(P_DIODE         , INPUT);
  pinMode(P_XBEE_SLEEP_RQ , OUTPUT);
  pinMode(P_XBEE_CTS      , INPUT);
  pinMode(P_XBEE_RESET    , OUTPUT);
 
  digitalWrite(P_LED, 1);

  //Configure isr for LED pin
  attachInterrupt(0, isrFlash, RISING);

  //WakeUp
  xbeeReset();
  xbeeWakeUp();
  xbeeWaitForReady();
  
  //Power down unused modules
  power_adc_disable();
  power_spi_disable();
  power_timer1_disable();
  power_twi_disable();
  //TODO see if there is more to disable
  
  //Configure IDLE mode, not deeper mode because need to have timer2 running
  set_sleep_mode(SLEEP_MODE_IDLE);
  
   //Configure timer isr for sending counter to xbee
  MsTimer2::set(59868, isrTimer2);
  MsTimer2::start();
 
  // Startup delay to wait for XBee radio to initialize.
  delay(10000);
  
  //Configure xbee serial 
  Serial.begin(9600);
  xbee.setSerial(Serial);
}


//---------------------
// Arduino loop
//---------------------
void loop() {

  if(readyToSend==true) {
    xbeeWakeUp();

    // Will try to send NB_TX_TRY times max, if still failing will try again after the next Timer2 interrupt
    TxTry=NB_TX_TRY;
    while((TxTry>0) && ( !queue.isEmpty() || TxPending==true)) {
      TxTry--;
      while(readPacket());  //read until timeout occurs
      //Pop from queue is no data pending
      if(TxPending==false) {
        payload.msgType = SH_SMARTMETER_SAMPLE;
        payload.msgID   = msgID;
        payload.nbSlot  = 1;  //TODO must be within the queue
        payload.wattCnt = queue.pop();
        memcpy(&buffer, &payload, sizeof(buffer));
      }

      //Send message
      debug.print("Send message... ");
      TxSuccess=false;
      TxPending=true;
      xbeeWaitForReady();
      for (int i = 0; i < sizeof(buffer); i++) {
         debug.print(buffer[i], HEX);
         debug.print(" ");
      }
      xbee.send(dataTx);

      //Check status response
      if (xbee.readPacket(2000)) {
        if (xbee.getResponse().isAvailable()) {
          if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
            xbee.getResponse().getZBTxStatusResponse(txStatus);
            if (txStatus.getDeliveryStatus() == SUCCESS)
              TxSuccess=true;
          } 
        } else if (xbee.getResponse().isError()) {
          debug.print("XBee error. error code is");
          debug.println(xbee.getResponse().getErrorCode(), DEC);
        }
      }

      //Check ACK from receiver
      check_ok = -1;
      if(TxSuccess==true) {
        debug.print("check ack... ");
        if (xbee.readPacket(5000)) {
          if (xbee.getResponse().isAvailable()) {
            if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
             for (int i = 0; i < xbee.getResponse().getFrameDataLength(); i++) {
                debug.print(xbee.getResponse().getFrameData()[i], HEX);
                debug.print(" ");
              }
              xbee.getResponse().getZBRxResponse(rx);
              if(rx.getData()[0]==SH_RECEIVER_ACK) {
                //Check it contents the msgID we sent
                check_ok  = 0;
                check_ok += rx.getData()[1] != (msgID & 0xff);
                check_ok += rx.getData()[2] != ((msgID>>8) & 0xff);
              }
            }
          } else if (xbee.getResponse().isError()) {
            debug.print("XBee error. error code is");
            debug.println(xbee.getResponse().getErrorCode(), DEC);
          }
        }
      }

      if(check_ok==0) {
        debug.println(" success!");
        TxPending=false;
        TxTry=NB_TX_TRY;
        msgID++;
      } else {
        debug.println(" failed!");
        if(TxTry!=0)
          delay(5000); //wait before trying again
        else
          nbFail++;
      }
    }
    //Reset xbee if too many fails
    if(nbFail>=NB_CONSECUTIVE_FAIL) {
      debug.print("Reset xbee...");
      xbeeReset();
      xbeeWaitForReady();
      debug.println(" done.");
      nbFail=0;
    }
    readyToSend=false;
    //Check if received any extra data
    while(readPacket());  //read until timeout occurs
    xbeeSleep();
  } 
  //CPU enter sleep now
  sleep_enable();
  sleep_mode();

  //Waking-up... disable sleep
  sleep_disable();
}


bool readPacket() {
  
  bool ret;

  ret=xbee.readPacket(3000);

  if (xbee.getResponse().isAvailable()) {
    debug.print("API=");
    debug.print(xbee.getResponse().getApiId(), HEX);
    debug.print(" : ");
    
    if (xbee.getResponse().getApiId() == ZB_RX_RESPONSE) {
      xbee.getResponse().getZBRxResponse(rx);
      for (int i = 0; i < xbee.getResponse().getFrameDataLength(); i++) {
        debug.print(xbee.getResponse().getFrameData()[i], HEX);
        debug.print(" ");
      }
    }
    debug.println("");
  } else if (xbee.getResponse().isError()) {
    debug.print("XBee error. error code is");
    debug.println(xbee.getResponse().getErrorCode(), DEC);
  }

  return ret;
}

